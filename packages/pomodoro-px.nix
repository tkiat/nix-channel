with import <nixpkgs> { };

stdenv.mkDerivation rec {

  pname = "pomodoro-px";
  version = "1.0.0";

  src = fetchgit {
    url = "https://github.com/tkiat/pomodoro-px.git";
    rev = "460842f66804d6c045aabbd55e09e213bef6d82a";
    sha256 = "1304kb11swwi1rh3qc7g97953iklwwi7rgxn5ga2pwym254v0vrj";
  };

  buildInputs = [
    (pkgs.python39.withPackages (ps: [ ]))
  ];

  installPhase = ''
    mkdir -p $out/bin
    cp $src/pomodoro-px.py $out/bin/$pname
    chmod +x $out/bin/$pname
  '';

  meta = with lib; {
    homepage = "https://github.com/tkiat/pomodoro-px";
    description = "A pausable and configurable pomodoro timer with records for X using Python.";
    license = licenses.gpl2Only;
    maintainers = [ "Theerawat Kiatdarakun" ];
    platforms = platforms.linux;
  };
}
