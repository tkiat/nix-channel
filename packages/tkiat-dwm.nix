with import <nixpkgs> { };

stdenv.mkDerivation rec {

  libX11 = pkgs.xorg.libX11;
  libXinerama = pkgs.xorg.libXinerama;
  libXft = pkgs.xorg.libXft;

  pname = "tkiat-dwm";
  version = "1.0.0";

  src = fetchgit {
    url = "https://gitlab.com/tkiat/forked-dwm.git";
    rev = "60f8de613fb4759a3dfb4ff90f452a46e37ec9a6";
    sha256 = "0slxljix450ppvlw1jqshxlq4n8fbd1irwa2xa65n4738wijijr3";
  };

  buildInputs = [ libX11 libXinerama libXft ];

  prePatch = ''
    sed -i "s@/usr/local@$out@" config.mk
  '';

  makeFlags = [ "CC=${stdenv.cc.targetPrefix}cc" ];

  meta = with lib; {
    homepage = "https://dwm.suckless.org/";
    description = "tkiat version of dwm: An extremely fast, small, and dynamic window manager for X";
    license = licenses.mit;
    maintainers = [ "Theerawat Kiatdarakun" ];
    platforms = platforms.linux;
  };
}
