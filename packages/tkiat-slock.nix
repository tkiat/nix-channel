with import <nixpkgs> { };

stdenv.mkDerivation rec {

  xorgproto = pkgs.xorg.xorgproto;
  libX11 = pkgs.xorg.libX11;
  libXext = pkgs.xorg.libXext;
  libXrandr = pkgs.xorg.libXrandr;

  pname = "tkiat-slock";
  version = "1.0.0";

  src = fetchgit {
    url = "https://gitlab.com/tkiat/forked-slock.git";
    rev = "33f92e433732247395b9fb0d9d7336416a0057eb";
    sha256 = "0s43rliyc761s5xxaxc7sfrqx58y069801l14cda0h6fcz94416p";
  };

  buildInputs = [ xorgproto libX11 libXext libXrandr ];

  installFlags = [ "DESTDIR=\${out}" "PREFIX=" ];

  postPatch = "sed -i '/chmod u+s/d' Makefile";

  meta = with lib; {
    homepage = "https://tools.suckless.org/slock";
    description = "tkiat version of slock: Simple X display locker";
    license = licenses.mit;
    maintainers = [ "Theerawat Kiatdarakun" ];
    platforms = platforms.linux;
  };
}
