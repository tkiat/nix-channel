# tkiat nix-channel

This channel contains both packages I author and packages I personalize. The latter has prefix `tkiat-`.

## Usage

```
nix-channel --add https://gitlab.com/tkiat/nix-channel/-/archive/main/nix-channel-main.tar.gz tkiat
nix-channel --update
nix-env -f '<tkiat>' -qa --description
```
