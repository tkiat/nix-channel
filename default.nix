let
  pinned-nixpkgs = builtins.fetchTarball {
    name = "nixos-21.05.2734.74d017edb67";
    url = https://releases.nixos.org/nixos/21.05/nixos-21.05.2734.74d017edb67/nixexprs.tar.xz;
    sha256 = "5c8f00a99fbb3c942888f14f2181d4864a2cfb8219e2b66ee151c3cf93cecf0a";
  };
in
with (import pinned-nixpkgs { });
rec {
  pomodoro-px = import ./packages/pomodoro-px.nix;

  tkiat-dmenu = import ./packages/tkiat-dmenu.nix;
  tkiat-dwm = import ./packages/tkiat-dwm.nix;
  tkiat-slock = import ./packages/tkiat-slock.nix;
  tkiat-st = import ./packages/tkiat-st.nix;
}
